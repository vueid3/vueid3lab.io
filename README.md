This repository hosts the source code for VueID3 mp3 tag editor, available at [vueid3.gitlab.io](vueid3.gitlab.io)

---

![src/assets/screenshots.png](src/assets/screenshots.png)
