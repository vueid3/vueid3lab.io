const tagNames = {
  Title: "TIT2",
  Artist: "TPE1",
  Album: "TALB",
  Year: "TYER",
  Comment: "COMM",
  Track: "TRCK",
  Genre: "TCON",
  Lyrics: "USLT"
};
let reverseObject = obj =>
  Object.fromEntries(Object.entries(obj).map(item => [item[1], item[0]]));
const tagDescriptions = reverseObject(tagNames);
const supportedTags = [
  "TPE1",
  "TCOM",
  "TCON",
  "TIT2",
  "TALB",
  "TPE2",
  "TPE3",
  "TPE4",
  "TRCK",
  "TPOS",
  "TPUB",
  "TKEY",
  "TMED",
  "TSRC",
  "TCOP",
  "WCOM",
  "WCOP",
  "WOAF",
  "WOAR",
  "WOAS",
  "WORS",
  "WPAY",
  "WPUB",
  "TLEN",
  "TDAT",
  "TYER",
  "TBPM",
  "COMM",
  "USLT",
  "TXXX"
];
const tagOrder = [
  "Lyrics",
  "Comment",
  "Year",
  "Genre",
  "Track",
  "Album",
  "Artist",
  "Title"
];
const tagsWithDescriptions = supportedTags
  .map(e => [e, tagDescriptions[e] || e])
  .sort((fst, snd) =>
    Math.sign(tagOrder.indexOf(snd[1]) - tagOrder.indexOf(fst[1]))
  );
const tagTemplates = {
  COMM: {
    description: ""
  },
  USLT: {
    description: ""
  },
  TXXX: {
    description: ""
  },
  APIC: {
    description: "",
    useUnicodeEncoding: false,
    type: 3
  }
};
const tagFields = {
  USLT: "lyrics",
  COMM: "text",
  APIC: "data",
  TXXX: "data"
};
const tagLists = ["TPE1", "TCOM", "TCON"];

function sortByKey(dict) {
  let unsortedDict = Object.entries(dict).map(i => [i[1].sortingKey, i[0]]);
  let sortedDict = unsortedDict.sort((fst, snd) => Math.sign(fst[0] - snd[0]));
  return sortedDict.map(i => i[1]);
}

export {
  tagNames,
  reverseObject,
  tagDescriptions,
  supportedTags,
  tagOrder,
  tagTemplates,
  tagFields,
  tagLists,
  sortByKey,
  tagsWithDescriptions
};
