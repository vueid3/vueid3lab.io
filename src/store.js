import * as localForage from "localforage";
import * as jsmediatags from "jsmediatags/dist/jsmediatags.min.js";
import * as fileType from "file-type";
import Vue from "vue";
import Vuex from "vuex";
import { ToastProgrammatic } from "buefy";
Vue.use(Vuex);

import { tagFields, supportedTags } from "./constants.js";

function normalizeTags(input) {
  let tags = input.tags;
  tags = Object.fromEntries(
    Object.entries(tags)
      .filter(([, value]) => value.data)
      .map(([key, value]) => [key, value.data])
      .map(([key, value]) => {
        if (key == "APIC") {
          return [key, new Uint8Array(value.data)];
        } else if (Object.keys(tagFields).includes(key)) {
          return [key, value[tagFields[key]]];
        } else {
          return [key, value];
        }
      })
      .filter(([key, value]) => value !== "" && key !== "picture")
  );
  return tags;
}

const store = new Vuex.Store({
  state: {
    fileDict: {},
    coverArtBuffer: new Uint8Array()
  },
  mutations: {
    updateFileDict(state, payload) {
      if (!(payload[0] in state.fileDict)) {
        Vue.set(state.fileDict, payload[0], {});
      }
      Vue.set(state.fileDict[payload[0]], payload[1], payload[2]);
    },
    updateCoverArt(state, payload) {
      state.coverArtBuffer = payload;
    },
    setTag(state, payload) {
      if (!("tags" in state.fileDict[payload[0]])) {
        Vue.set(state.fileDict[payload[0]], "tags", {});
      }
      Vue.set(state.fileDict[payload[0]]["tags"], payload[1], payload[2]);
    },
    removeFileDict(state, payload) {
      Vue.delete(state.fileDict, payload);
    }
  },
  actions: {
    updateFileList(context, list) {
      list.map(file => {
        let reader = new FileReader();
        let fileKey = file.name + file.lastModified + file.size;
        let storageKey = "vueid3-" + fileKey;
        /*Vue.set(state.fileDict, file.name+file.lastModified+file.size, file)  */
        /*console.log(file)*/
        context.commit("updateFileDict", [fileKey, "file", file]);
        context.commit("updateFileDict", [fileKey, "fileKey", fileKey]);
        context.commit("updateFileDict", [
          fileKey,
          "desiredFilename",
          file.name
        ]);
        reader.onload = e => {
          if (
            fileType(e.target.result) !== undefined &&
            fileType(e.target.result).mime.startsWith("image/") &&
            context.state.coverArtBuffer.length === 0
          ) {
            context.dispatch("removeFile", fileKey);
            ToastProgrammatic.open({
              duration: 4000,
              message: file.name + " attached as cover art",
              position: "is-bottom"
            });
            context.commit("updateCoverArt", new Uint8Array(e.target.result));
            return;
          }
          if (
            fileType(e.target.result) !== undefined &&
            fileType(e.target.result).mime.startsWith("image/")
          ) {
            context.dispatch("removeFile", fileKey);
            ToastProgrammatic.open({
              duration: 4000,
              message:
                file.name +
                " is an image, but cover art is already attached, skipping...",
              position: "is-bottom"
            });
            return;
          }
          if (
            fileType(e.target.result) === undefined ||
            fileType(e.target.result).ext !== "mp3"
          ) {
            ToastProgrammatic.open({
              duration: 4000,
              message: file.name + " is not a mp3 file, skipping...",
              position: "is-bottom"
            });
            context.dispatch("removeFile", fileKey);
            return;
          }
          localForage
            .setItem(storageKey, e.target.result)
            .then(() =>
              context.commit("updateFileDict", [
                fileKey,
                "storageKey",
                storageKey
              ])
            );
          jsmediatags.read(file, {
            onSuccess: tags => {
              let normalizedTags = normalizeTags(tags);
              if (
                "APIC" in normalizedTags &&
                context.state.coverArtBuffer.length === 0
              ) {
                context.commit(
                  "updateCoverArt",
                  new Uint8Array(normalizedTags["APIC"])
                );
              }
              normalizedTags = Object.fromEntries(
                Object.entries(normalizedTags).filter(([key]) =>
                  supportedTags.includes(key)
                )
              );
              context.commit("updateFileDict", [
                fileKey,
                "tags",
                normalizedTags
              ]);
              context.dispatch("assignSortingKeys");
            },
            onError: function() {
              context.commit("updateFileDict", [fileKey, "tags", {}]);
              context.dispatch("assignSortingKeys");
            }
          });
        };
        reader.readAsArrayBuffer(file);
      });
    },
    clearFileList(context) {
      localForage.clear();
      context.commit("updateCoverArt", new Uint8Array());
      Object.entries(context.state.fileDict).map(([key, value]) => {
        localForage.removeItem(value.storageKey);
        context.commit("removeFileDict", key);
      });
    },
    removeFile(context, fileKey) {
      let storageKey = "vueid3-" + fileKey;
      context.commit("removeFileDict", fileKey);
      localForage.removeItem(storageKey);
      context.dispatch("assignSortingKeys");
    },
    assignSortingKeys(context) {
      let fileDict = context.state.fileDict;
      let fileList = Object.keys(fileDict)
        .filter(e => typeof fileDict[e].tags !== "undefined")
        .map(item => [
          typeof fileDict[item].tags["TRCK"] !== "undefined"
            ? parseInt(fileDict[item].tags["TRCK"]) - 1
            : undefined,
          item
        ]);
      let sortedPart = fileList
        .filter(item => item[0] !== undefined)
        .sort((fst, snd) => Math.sign(fst[0] - snd[0]));
      let unsortedPart = fileList.filter(item => item[0] === undefined);
      sortedPart.map((item, index) =>
        context.commit("updateFileDict", [item[1], "sortingKey", index])
      );
      unsortedPart.map((item, index) =>
        context.commit("updateFileDict", [
          item[1],
          "sortingKey",
          index + sortedPart.length
        ])
      );
    }
  }
});

export default store;
